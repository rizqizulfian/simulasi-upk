<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	if(Auth::check())
	{
		if(Auth::user()->hasRole('Admin')){
			return redirect('admin');
		}if(Auth::user()->hasRole('Member')){
			return redirect('member');
		}else{
			return redirect('home');
		}
	}else{
		return redirect('login');
	}
    
});

Route::auth();

Route::get('/home', 'HomeController@index');
Route::resource('anggota', 'AnggotaController');

/* Admin Routes*/
Route::get('admin', 'AdminController@admin');

/* Member Routes*/
Route::get('member', 'MemberController@member');
Route::get('profil', 'MemberController@profil');
