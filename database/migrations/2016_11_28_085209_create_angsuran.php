<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAngsuran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('angsuran', function (Blueprint $table) {
            $table->increments('id_angsuran');
            $table->integer('id_kategori');
            $table->integer('id_anggota');
            $table->date('tgl_pembayaran');
            $table->integer('angsuran_ke');
            $table->integer('besar_angsuran');
            $table->text('ket');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('angsuran');
    }
}
