<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePinjaman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjaman', function (Blueprint $table) {
            $table->increments('id_pinjaman');
            $table->string('nama_pinjaman');
            $table->integer('id_anggota');
            $table->integer('besar_pinjaman');
            $table->date('tgl_pengajuan_pinjaman');
            $table->date('tgl_acc_peminjam');
            $table->date('tgl_pinjaman');
            $table->date('tgl_penulusuran');
            $table->integer('id_angsuran');
            $table->text('ket');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('pinjaman');
    }
}
