<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetugasKoperasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petugas_koperasi', function (Blueprint $table) {
            $table->increments('id_petugas');
            $table->string('name');
            $table->string('alamat');
            $table->integer('no_tlp');
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->text('ket');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('petugas_koperasi');
    }
}
