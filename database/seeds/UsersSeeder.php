<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Membuat role admin
		$adminRole = new Role();
		$adminRole->name = "Admin";
		$adminRole->display_name = "Admin";
		$adminRole->save();
		// Membuat role member
		$memberRole = new Role();
		$memberRole->name = "Member";
		$memberRole->display_name = "Member";
		$memberRole->save();
		// Membuat sample admin
		$admin = new User();
		$admin->name = 'Admin';
		$admin->email = 'admin@gmail.com';
		$admin->password = bcrypt('123');
		$admin->save();
		$admin->attachRole($adminRole);
		// Membuat sample member
		$member = new User();
		$member->name = "Member";
		$member->email = 'member@gmail.com';
		$member->password = bcrypt('123');
		$member->save();
		$member->attachRole($memberRole);

		$member1 = new User();
		$member1->name = "Member2";
		$member1->email = 'member2@gmail.com';
		$member1->password = bcrypt('123');
		$member1->save();
		$member1->attachRole($memberRole);
    }
}
