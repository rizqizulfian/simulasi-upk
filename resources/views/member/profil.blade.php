@extends('layouts.member')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Profil</div>

                <div class="panel-body">
                    Welcome, {{ Auth::user()->name }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
