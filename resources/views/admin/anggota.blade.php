@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Anggota</div>

                <div class="panel-body">
                @foreach($users as $user)
                        {{ $user -> name }}<br>
                @endforeach<br><p>
                <a href="#"><font color="blue">+ Tambah Anggota</a></font>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection